<?php

namespace App\Http\Controllers;

//questo server per usare l'oggetto Request nei metodi
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoController extends Controller
{
    //aggiunge un nuovo todo chiamato dalla route TodoController@add
    public function add(Request $request)
    {
        $description = $request->input('description');
        $results = app('db')->insert(
            "insert into tasks(description,done,insertDate) values('$description',false,now())"
        );
        //ritorno 201 created
        return new Response(null, 201);
    }

    public function update(Request $request,$id)
    {
        $description = $request->input('description');
        $done = $request->input('done');
        $results = app('db')->update(
            "UPDATE tasks SET description='$description', done=$done WHERE id=$id ",
        );

    }

    public function delete(Request $request,$id)
    {
        $results = app('db')->delete( "UPDATE FROM tasks WHERE  id=$id ");

        //ritorno 204 no conten
        return new Response(null, 204);

    }
}
