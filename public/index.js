//questa funzione mi aggiunge il mio bel todo
//dentro il db del prof Masetti all'indirizzo 10.
//usando il mio web server lumen
async function addTodo(todoDescription){
    //devo fare chiamata HTTP post che simula quello che fa postman
    var newTodo = {
        description: todoDescription
    }

    await fetch('http://localhost:8080/api/todos',{
        method:'POST',
        headers:{'ContentType':'application/json'},
        body: JSON.stringify(newTodo)
    })
}

console.log('javascript pronto')

//async = operazione asincrona(non"spezzato")
async function main(){
    console.log('running main')
    //prendo il tag div con id app
    var divApp=document.getElementById('app');
    //creo un tag title
    var title=document.createElement('h1');
    title.textContent="TODO APP"
    //aggiungo il tag dentro la div
    divApp.appendChild(title);
    //prendo i todos via HTTP con la funzione fetch
    //la parola await permette di rendere sincrona un'operazione asincrona
    var todosResponse=await fetch('http://localhost:8080/api/todos');
    var todosJson=await todosResponse.json();
    //creo l'url
    console.log(todosJson);
    var textInput=document.createElement('input');
    textInput.placeholder='Inserisci un nuovo todo';
    //aggiungo id all'input
    textInput.id='addTodoInput';
    divApp.appendChild(textInput);
    //aggiungo il bottone aggiungi 
    var addButton=document.createElement('button');
    addButton.textContent='Aggiungimi';
    divApp.appendChild(addButton);
    //creo la lista UL in javascript
    addButton.addEventListener('click',function(){
        //recupero la descrizione del nuovo todo
        var todoDesc=document.getElementById('addTodoInput').value;
        //uso la funzione addTodo che definisco sopra
        addTodo(todoDesc); 
    })
    var ulList=document.createElement('ul');
    //aggiungo la lista alla fine del container div#app
    divApp.appendChild(ulList);

    //itero gli elementi e li aggiungo all'UL
    for(var i=0; i < todosJson.length; i++){
        //creo il tag LI
        var liElement=document.createElement('li');
        //creo una variabile con l'elemento i-esimo della lista (il singolo)
        var todo=todosJson[i];
        //metto la descrizione come contenuto
        liElement.textContent='#'+todo.id+''+todo.description;
        liElement.textContent+='(';
        if(todo.done){
            liElement.textContent+='Completato';
        }
        else{
            liElement.textContent+='Da completare';
        }
        liElement.textContent+=')';
        //lo approdiamo
        ulList.appendChild(liElement);
    }
}


//associo l'evento DOMContentLoaded alla funzione main
document.addEventListener("DOMContentLoaded",function() {
    main()
});